FROM ubuntu:16.04
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

# Based on work by Fer Uria - https://github.com/fauria/docker-lamp

ENV LOG_STDOUT **Boolean**
ENV LOG_STDERR **Boolean**
ENV LOG_LEVEL warn
ENV ALLOW_OVERRIDE All
ENV DATE_TIMEZONE UTC
ENV TERM dumb

COPY debconf.selections /tmp/
RUN debconf-set-selections /tmp/debconf.selections \
    && apt-get update \
    && apt-get install -y software-properties-common \
    && LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php \
    && apt-get update \
    && TERM=xterm apt-get install -y \
        php7.1 \
        php7.1-bz2 \
        php7.1-cgi \
        php7.1-cli \
        php7.1-common \
        php7.1-curl \
        php7.1-dev \
        php7.1-enchant \
        php7.1-fpm \
        php7.1-gd \
        php7.1-gmp \
        php7.1-imap \
        php7.1-interbase \
        php7.1-intl \
        php7.1-json \
        php7.1-ldap \
        php7.1-mbstring \
        php7.1-mcrypt \
        php7.1-mysql \
        php7.1-odbc \
        php7.1-opcache \
        php7.1-pgsql \
        php7.1-phpdbg \
        php7.1-pspell \
        php7.1-readline \
        php7.1-recode \
        php7.1-sqlite3 \
        php7.1-sybase \
        php7.1-tidy \
        php7.1-xmlrpc \
        php7.1-xsl \
        php7.1-zip \
# Apache
        apache2 \
        libapache2-mod-php7.1 \
# MariaDB
        mariadb-common \
        mariadb-server \
        mariadb-client \
# Others
        zip \
        unzip \     
        postfix \
        git \
        nodejs \
        npm \
        nano \
        tree \
        vim \
        curl \
        ftp \
        pv \
# Cleanup
    && rm -rf /var/lib/apt/lists/* \
# Node packages
    && npm install -g bower grunt-cli gulp \
# Allow MySQL connections from outside
    && sed -i "s/bind-address/#bind-address/g" /etc/mysql/mariadb.conf.d/50-server.cnf \
# Install Composer
    && cd /usr/bin \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
# Various preparations
    && a2enmod rewrite \
    && ln -s /usr/bin/nodejs /usr/bin/node \
    && chown -R www-data:www-data /var/www/html \
    && mkdir -p /var/log/httpd && chmod -R 0777 /var/log/httpd \
    && mkdir -p /var/log/mysql && chmod -R 0777 /var/log/mysql \
    && mkdir /import

VOLUME /import
VOLUME /var/www/html
VOLUME /var/log/httpd
VOLUME /var/lib/mysql
VOLUME /var/log/mysql
VOLUME /etc/apache2

COPY index.php /var/www/html/
COPY run-lamp.sh /usr/sbin/
RUN chmod +x /usr/sbin/run-lamp.sh

EXPOSE 80
EXPOSE 3306

CMD ["/usr/sbin/run-lamp.sh"]
