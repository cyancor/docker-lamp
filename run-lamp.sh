#!/bin/bash

function exportBoolean {
    if [ "${!1}" = "**Boolean**" ]; then
            export ${1}=''
    else 
            export ${1}='Yes.'
    fi
}

exportBoolean LOG_STDOUT
exportBoolean LOG_STDERR

if [ $LOG_STDERR ]; then
    /bin/ln -sf /dev/stderr /var/log/apache2/error.log
else
	LOG_STDERR='No.'
fi

if [ $ALLOW_OVERRIDE == 'All' ]; then
    /bin/sed -i 's/AllowOverride\ None/AllowOverride\ All/g' /etc/apache2/apache2.conf
fi

if [ $LOG_LEVEL != 'warn' ]; then
    /bin/sed -i "s/LogLevel\ warn/LogLevel\ ${LOG_LEVEL}/g" /etc/apache2/apache2.conf
fi

if [ $DocumentRoot != "" ]; then
    echo "Setting document root to $DocumentRoot..."
    DocumentRootMasked=${DocumentRoot//\//\\\/}
    /bin/sed -i "s/\/var\/www\/html/$DocumentRootMasked/g" /etc/apache2/sites-available/000-default.conf
fi


# enable php short tags:
/bin/sed -i "s/short_open_tag\ \=\ Off/short_open_tag\ \=\ On/g" /etc/php/7.0/apache2/php.ini

# stdout server info:
if [ ! $LOG_STDOUT ]; then
cat << EOB
    
    **********************************************
    *                                            *
    *    Docker image: fauria/lamp               *
    *    https://github.com/fauria/docker-lamp   *
    *                                            *
    **********************************************

    SERVER SETTINGS
    ---------------
    · Redirect Apache access_log to STDOUT [LOG_STDOUT]: No.
    · Redirect Apache error_log to STDERR [LOG_STDERR]: $LOG_STDERR
    · Log Level [LOG_LEVEL]: $LOG_LEVEL
    · Allow override [ALLOW_OVERRIDE]: $ALLOW_OVERRIDE
    · PHP date timezone [DATE_TIMEZONE]: $DATE_TIMEZONE

EOB
else
    /bin/ln -sf /dev/stdout /var/log/apache2/access.log
fi

# Set PHP timezone
/bin/sed -i "s/\;date\.timezone\ \=/date\.timezone\ \=\ ${DATE_TIMEZONE}/" /etc/php/7.0/apache2/php.ini

# Run Postfix
/usr/sbin/postfix start

# Run MySQL
service mysql start
# /usr/bin/mysqld_safe --no-defaults

# Wait for MariaDB to be up and running
echo "Waiting for MySQL to be up and running..."
while ! mysqladmin ping --silent; do
    sleep 1
done

# Allow root connections from outside
mysql -u root -p12345 -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '12345' WITH GRANT OPTION;"
# Add client user
mysql -u root -p12345 -e "CREATE DATABASE main;"
mysql -u root -p12345  -e "CREATE USER 'client' IDENTIFIED BY '12345';"
mysql -u root -p12345  -e "GRANT USAGE ON *.* TO 'client'@localhost IDENTIFIED BY '12345';"
mysql -u root -p12345  -e "GRANT USAGE ON *.* TO 'client'@'%' IDENTIFIED BY '12345';"
mysql -u root -p12345  -e "GRANT ALL privileges ON main.* TO 'client'@localhost;"
mysql -u root -p12345  -e "GRANT ALL privileges ON main.* TO 'client'@'%';"
mysql -u root -p12345  -e "FLUSH PRIVILEGES;"

find /import/*.sql | while read file; do
    echo "Importing $file..."
    pv "$file" | mysql -u root -p12345 main
done

if [ -f /import/php.ini ]; then
    echo "Importing php.ini file..."
    cp /import/php.ini /etc/php/7.1/apache2/php.ini
fi

echo "Import done."


# Run Apache:
if [ $LOG_LEVEL == 'debug' ]; then
    /usr/sbin/apachectl -DFOREGROUND -k start -e debug
else
    &>/dev/null /usr/sbin/apachectl -DFOREGROUND -k start
fi
